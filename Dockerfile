# Dockerfile
# use an Ubuntu+CUDA official image as base
FROM mambaorg/micromamba:1.4.2-focal-cuda-11.7.1

USER root
RUN apt-get update && apt-get -y install sudo gcc git

# setting locales
ENV TZ=Europe/Zurich

# installing the necessary Ubuntu and Python packages
# in a single RUN command, to reduce the number of layers
# in our image. (Every Dockerfile command creates a compressed layer)
# RUN apt-get update && apt-get install -y --no-install-recommends \
#    git \
#    build-essential \
#    cmake \
#    apt-get clean && \
#    rm -rf /var/lib/apt/lists/*

# copy requirements.txt file inside the image and
# pip install everything
COPY requirements.txt requirements.txt
COPY env.yaml env.yaml
RUN micromamba install -y -n base -f env.yaml && \
    eval "$(micromamba shell hook --shell=bash)" && \
    micromamba activate base && \
    pip install --no-cache-dir -r requirements.txt && \
    mkdir cupycache && \
    export CUPY_CACHE_DIR=./cupycache

ENV CUPY_CACHE_DIR ./cupycache
