import numpy as np

interval_param_space_1 = [0.1, 0.1, 0.1, 0.1,
                          0.15, 0.15, 0.15, 0.15,
		 		          0.2, 0.2, 0.2, 0.2,
				          0.3, 0.3, 0.3, 0.3,
				          0.4, 0.4, 0.4, 0.4,
				          0.5, 0.5, 0.5, 0.5,
				          0.6, 0.6, 0.6, 0.6,
				          0.7, 0.7, 0.7, 0.7,
				          0.8, 0.8, 0.8, 0.8,
				          0.9, 0.9, 0.9, 0.9,
				          1.0, 1.0, 1.0, 1.0]

interval_param_space_2 = [1.5, 1.5, 1.5, 1.5,
				          2, 2, 2, 2,
				          3, 3, 3, 3,
				          4, 4, 4, 4,
				          5, 5, 5, 5,
				          6, 6, 6, 6,
				          7, 7, 7, 7,
				          8, 8, 8, 8,
				          9, 9, 9, 9,
				          10, 10, 10, 10]

interval_param_space = sorted(list(set(np.append(interval_param_space_1, interval_param_space_2))))

gain_param_space = np.arange(0.05, 1, 0.05)

reps_param_space = [1000]

with open('args.txt', 'w') as f:
    for interval in interval_param_space:
        for gain in gain_param_space:
            for reps in reps_param_space:
                f.write(str(round(gain, 2)) + ' ' + str(round(interval, 2)) + ' ' + str(reps) + '\n')